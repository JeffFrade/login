<?php
	
	require_once __DIR__.'/../src/Service/ServiceLogin.php';
	
	$login = new ServiceLogin();
	
	if (!isset($_SESSION)) {
		session_start();
	}
	
	$login->login($_POST);
	
	if (!empty($_SESSION['name'])) {
		header("Location: admin.php");
	} else {
		header("Location: ../index.php");
	}
