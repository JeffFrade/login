<?php

	if (!isset($_SESSION)) {
		session_start();
	}
	
	if (!isset($_SESSION['name']) || empty($_SESSION['name'])) {
		header("Location: ../index.php");
	}
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8"/>
		<title>Login</title>
	</head>
	
	<body>
		<h1>Logado</h1>
		<hr/>
		<a href="logout.php">Logout</a>
	</body>
</html>