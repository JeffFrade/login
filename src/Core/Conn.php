<?php

class Conn
{
    public static function connect($host, $dbname, $user, $pass)
    {
        try {
            return new \PDO("mysql:host=".$host.";dbname=".$dbname, $user, $pass);
        } catch (\PDOException $e) {
            return $e->getCode()." ".$e->getMessage();
        }
    }
}
