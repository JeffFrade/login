<?php
	
	require_once __DIR__.'/AbstractTable.php';
	
	class LoginTable extends AbstractTable
	{
		public function __construct()
		{
			$this->table = 'tb_login';
			// parent => Referência a classe pai
			parent::__construct();
		}
		
		public function login($params)
		{
			$sql = "SELECT user, pass FROM ".$this->table." WHERE user = :user AND pass = :pass";
			
			// Criando um Statment e preparando a query:
			$stmt = $this->db->prepare($sql);
			
			// Adicionando os Parâmetros:
			$stmt->bindValue(':user', $params['user']);
			$stmt->bindValue(':pass', $params['pass']);
			
			// Executando a Query:
			$stmt->execute();
			
			// Fazendo o fetch para array associativo:
			$data = $stmt->fetch(\PDO::FETCH_ASSOC);
			
			if (!empty($data['user']) && !empty($data['pass'])) {
				$_SESSION['name'] = $params['user'];
				$this->updateTime();
				return true;
			}
			
			return false;
		}
		
		public function updateTime()
		{
			// Setar o timezone para America/Sao_Paulo
			$sql = "UPDATE ".$this->table." SET created_at = :created_at";
			
			$stmt = $this->db->prepare($sql);
			
			$stmt->bindValue(':created_at', date('Y-m-d H:i:s'));
			
			$stmt->execute();
		}
	}
