<?php

	require_once __DIR__.'/../Table/LoginTable.php';

	class ServiceLogin
	{
		private $tableLogin;
		
		public function __construct()
		{
			$this->tableLogin = new LoginTable();
		}
		
		public function login($params)
		{
			$params['pass'] = md5($params['pass']);
			
			$this->tableLogin->login($params);
		}
	}
	