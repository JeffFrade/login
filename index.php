<?php
	
	if (!isset($_SESSION)) {
		session_start();
	}

	if (isset($_SESSION['name'])) {
		unset($_SESSION['name']);
	}
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8"/>
		<title>Login</title>
	</head>
	
	<body>
		<form id="frm_login" name="frm_login" method="post" action="pages/login.php">
			<label for="user">Usuário: </label>
			<input type="text" id="user" name="user" placeholder="Usuário"/>
			<br/>
			<label for="pass">Senha: </label>
			<input type="password" id="pass" name="pass" placeholder="Senha"/>
			<br/>
			<input type="submit" id="btn_login" name="btn_login" onclick="return validate()"/>
		</form>

		<script type="text/javascript">
			function validate() {
				let user = document.getElementById('user').value;
				let pass = document.getElementById('pass').value;
				
				if (user.trim() == '') {
					alert('Preencha o campo Usuário');
					document.getElementById('user').focus();
					return false;
				}
				
				if (pass.trim() == '') {
					alert('Preencha o campo Senha');
					document.getElementById('pass').focus();
					return false;
				}
				
				document.getElementById('frm_login').submit();
			}
		</script>
	</body>
</html>